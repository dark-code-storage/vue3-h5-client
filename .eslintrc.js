const { defineConfig } = require("eslint-define-config");

module.exports = defineConfig({
  root: true,
  env: {
    browser: true,
    node: true,
    commonjs: true,
    es6: true,
    amd: true,
    // 解决 defineProps and defineEmits generate no-undef warnings
    "vue/setup-compiler-macros": true
  },
  /* 指定如何解析语法。*/
  parser: "vue-eslint-parser",
  /* 优先级低于parse的语法解析配置 */
  parserOptions: {
    parser: "@typescript-eslint/parser",
    ecmaVersion: "latest",
    //模块化方案
    sourceType: "module",
    ecmaFeatures: {
      jsx: true
    }
  },
  extends: [
    "plugin:vue/vue3-recommended",
    "plugin:@typescript-eslint/recommended", // typescript-eslint推荐规则,
    "prettier",
    "plugin:prettier/recommended"
  ],
  plugins: ["prettier", "import", "unused-imports"],
  rules: {
    "no-unused-vars": "off",
    "import/no-duplicates": "error",
    "unused-imports/no-unused-imports": "warn",
    "@typescript-eslint/no-empty-interface": "off",
    "@typescript-eslint/no-unused-vars": ["off"],
    "@typescript-eslint/no-explicit-any": ["off"],
    "vue/multi-word-component-names": 0, // 取消组件名称校验
    "vue/no-v-html": "off",
    "vue/require-default-prop": "off",
    "vue/no-mutating-props": "off",
    "@typescript-eslint/no-non-null-assertion": 0,
    "@typescript-eslint/no-var-requires": 0,
    "@typescript-eslint/ban-types": "off",
    "@typescript-eslint/ban-ts-comment": "off",
    "@typescript-eslint/no-empty-function": "off",
    "@typescript-eslint/no-this-alias": "off",
    "vue/comment-directive": "off",
    "no-nested-ternary": 0,
    "comma-dangle": [2, "never"], //对象字面量项尾不能有逗号
    "comma-spacing": 0, //逗号前后的空格
    semi: [2, "always"], //语句强制分号结尾
    "prettier/prettier": [
      0,
      {},
      {
        usePrettierrc: false
      }
    ]
  },
  overrides: [
    {
      files: [
        "**/__tests__/*.{j,t}s?(x)",
        "**/tests/unit/**/*.spec.{j,t}s?(x)"
      ],
      env: {
        jest: true
      }
    }
  ]
});
