module.exports = {
  jsxSingleQuote: false, // jsx 不使用单引号，而使用双引号
  printWidth: 80, // 超过最大值换行
  tabWidth: 2, // 缩进字节数 使用 4 个空格缩进
  semi: true, // 句尾添加分号
  trailingComma: "none",
  overrides: [
    {
      files: "*.html",
      options: {
        parser: "html"
      }
    }
  ]
}
