import { createI18n } from "vue-i18n";
import zhLocale from "./zh";
import enLocale from "./en";
import { LocaleEnum } from "@/enums/optionsEnum";
import { getStorage } from "@/utils/storage";
import { LANGUAGE_KEY } from "@/constants/config";
const langKeyMap = {
  zh: LocaleEnum.zh,
  cn: LocaleEnum.zh,
  en: LocaleEnum.en
};
/**
 * @description 浏览器语言
 */
const browserLang = navigator.language.slice(0, 2) as LocaleEnum;
/**
 * @description 浏览器缓存语言
 */
const storageLang = (getStorage(LANGUAGE_KEY) ?? LocaleEnum.zh) as LocaleEnum;

export const initLang =
  storageLang || LocaleEnum[langKeyMap[browserLang]] || LocaleEnum.zh;
const i18n = createI18n({
  locale: initLang,
  fallbackLocale: initLang,
  messages: {
    zh: zhLocale,
    en: enLocale
  }
});

export default i18n;
