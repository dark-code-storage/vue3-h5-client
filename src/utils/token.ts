import { setStorage, getStorage, removeStorage } from "./storage";
import { TOKEN_KEY } from "@/constants/config";

class Token {
  public static clearToken() {
    removeStorage(TOKEN_KEY);
  }
  public static setToken(data: string) {
    setStorage(TOKEN_KEY, data);
  }
  public static getToken() {
    return getStorage(TOKEN_KEY);
  }
}

export default Token;
