/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path: string) {
  return /^(https?:|mailto:|tel:)/.test(path);
}

/**
 * @description: 判断数据类型 ;
 * @param {*}
 * @return {*}
 */

export const isObject = (value: unknown): value is Record<any, any> =>
  value !== null && Object.prototype.toString.call(value) === "[object Object]";

export const isFunction = (value: unknown): value is Function =>
  typeof value === "function";

export const isString = (value: unknown): value is string =>
  typeof value === "string";

export const isBoolean = (value: unknown): value is boolean =>
  typeof value === "boolean";

export const isNumber = (value: unknown): value is number =>
  typeof value === "number";

export const isUndef = (value: unknown): value is undefined =>
  typeof value === "undefined";

export const isArray = (value: unknown): value is Array<any> =>
  value !== null && Object.prototype.toString.call(value) === "[object Array]";

export function isDate(val: unknown): val is Date {
  return (
    Object.prototype.toString.call(val) === "[object Date]" &&
    !isNaN((val as Date).getTime())
  );
}

/**
 * @Description:  是否是手机设备;
 * @Param:  ;
 * @Return:  ;
 */

export const isMobile = () => {
  const regMobileAll =
    /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i;
  return regMobileAll.test(window.navigator.userAgent);
};

export const isAndroid = () => {
  return /android/i.test(navigator.userAgent.toLowerCase());
};

export const isIOS = () => {
  const reg = /iPhone|iPad|iPod|iOS|Macintosh/i;
  return reg.test(navigator.userAgent.toLowerCase());
};
