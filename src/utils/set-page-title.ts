import { PAGE_DEFAULT_TITLE } from "@/constants/config";
// 修改标题
export default function setPageTitle(routerTitle?: string): void {
  window.document.title = routerTitle
    ? `${routerTitle} | ${PAGE_DEFAULT_TITLE}`
    : `${PAGE_DEFAULT_TITLE}`;
}
