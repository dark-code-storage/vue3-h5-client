import { useConfigStoreHook } from "@/store/modules/config";

export function useThemeMode() {
  return useConfigStoreHook().themeMode;
}

export function useToggleThemeMode() {
  useConfigStoreHook().toggleThemeMode();
}
