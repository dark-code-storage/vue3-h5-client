import type { LocaleEnum } from "@/enums/optionsEnum";
import { useConfigStoreHook } from "@/store/modules/config";

export function useLanguage() {
  return useConfigStoreHook().language;
}

export function useToggleLanguage(lang: LocaleEnum) {
  useConfigStoreHook().toggleLanguage(lang);
}
