/**
 * @description: ws 接收事件
 */
export enum WSReceptionEnum {
  /** 成功连接 */
  RECEPTION_EVENT_ONLINE = "online",
  /** 断开连接 */
  RECEPTION_EVENT_OFFLINE = "offline",
  /** 接收通知 */
  RECEPTION_EVENT_NOTICE = "notice",
}

/**
 * @description: ws 发送事件
 */
export enum WSDispatchEnum {
  /** 发送通知 */
  DISPATCH_EVENT_NOTICE = "notice", //公告
}
