/**
 * @description: 主题
 */
export enum ThemeEnum {
  DARK = "dark",
  LIGHT = "light",
}

/**
 * @description: 语言
 */
export enum LocaleEnum {
  zh = "zh",
  en = "en",
}

export enum LangEnum {
  zh = "简体中文",
  en = "English",
}
