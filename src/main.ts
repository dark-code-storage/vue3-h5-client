import { createApp } from "vue";
// normalize.css
import "normalize.css/normalize.css";
// 全局样式
import "./assets/styles/index.less";
// tailwindcss
import "./assets/styles/tailwind.css";
// svg icon
import "virtual:svg-icons-register";

import { store } from "./store";
import router from "./router";
import App from "./App.vue";
import ws from "./plugins/ws";
import i18n from "./locales/index";
import directives from "./plugins/directives";

const app = createApp(App);

app.use(store).use(router);

// 注册插件
app.use(directives).use(ws).use(i18n);

app.mount("#app");
