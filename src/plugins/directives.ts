import type { App, DirectiveBinding } from "vue";
import { isUndef } from "@/utils/validate";

/**
 * 自定义指令
 * @description 保证插件单一职责，当前插件只用于添加自定义指令
 */
export default {
  install: (app: App) => {
    /**
     * @directive v-debounce
     * @description 用户按钮点击、或者输入框 防抖
     * @tips 默认防抖时间为 1000 毫秒
     * @example <input v-debounce:[2000].click.input="handleInput" />
     */
    app.directive(
      "debounce",
      (el: HTMLElement, binding: DirectiveBinding<() => void>) => {
        let time = 1000; // 默认 1000毫秒
        if (!isUndef(binding.arg)) {
          time = Number(binding.arg);
        }
        // 修饰符为需绑定的事件名
        const selectedModifiers = Object.keys(binding.modifiers).filter(
          (key) => binding.modifiers[key]
        );
        if (selectedModifiers.length === 0) {
          // 添加默认事件'click'
          selectedModifiers.push("click");
        }
        // 循环绑定事件
        selectedModifiers.map((event) => {
          let timeout: number | NodeJS.Timeout;

          el.addEventListener(event, () => {
            clearTimeout(timeout);
            timeout = setTimeout(() => {
              binding.value();
            }, time);
          });
        });
      }
    );

    /**
     * 权限控制
     * @description 用于在功能按钮上绑定权限，没权限时会销毁或隐藏对应 DOM 节点
     * @tips 指令传入的值是管理员的组别 id
     * @example <div v-permission="1" />
     */
    app.directive("permission", (el, binding: DirectiveBinding) => {
      // 假设 1 是管理员组别的 id ，则无需处理
      if (binding.value === 1) return;

      // 其他情况认为没有权限，需要隐藏掉界面上的 DOM 元素
      if (el.parentNode) {
        el.parentNode.removeChild(el);
      } else {
        el.style.display = "none";
      }
    });
  }
};
