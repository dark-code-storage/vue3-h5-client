import type { App } from "vue";
import io from "socket.io-client";
import { getStorage } from "@/utils/storage";
import type { Socket } from "socket.io-client";
import { WS_PATH, WS_URL } from "@/constants/config";
import { WSDispatchEnum, WSReceptionEnum } from "@/enums/wsEnum";
/**
 * @description 连接ws
 * @link https://socket.io/
 */
class WS {
  constructor() {
    this.init();
  }
  private _socket: Socket | undefined;

  private async init() {
    this._socket = io(WS_URL, {
      reconnection: true,
      path: WS_PATH,
      transports: ["websocket", "polling"], //优先websocket
      query: {
        token: await getStorage("token")
      },
      reconnectionAttempts: 10 //重连次数
    });

    this._socket?.on(WSReceptionEnum.RECEPTION_EVENT_ONLINE, () => {
      // console.log(WS_URL + "成功连接");
    });

    this._socket?.on(WSReceptionEnum.RECEPTION_EVENT_OFFLINE, () => {
      // console.log(WS_URL + "断开连接");
    });

    // 当发出任何事件时将触发该侦听器。
    // this._socket.onAny((eventName, ...args) => {
    //   console.log(eventName);
    // });

    // 当发出任何事件时将触发该侦听器
    // this._socket.prependAny((eventName, ...args) => {
    //   // ...
    // });
  }

  /**
   * @description 订阅一个ws事件
   * @param eventName ws标识
   * @param fn 回调函数
   */
  public readonly subscribe = (eventName: WSReceptionEnum, fn: () => void) => {
    if (this._socket && this._socket.connected) {
      this._socket.on(eventName, fn);
    } else {
      console.log("ws未连接");
    }
  };

  /**
   * @description 取消订阅一个ws事件
   * @param eventName ws标识
   * @param fn 回调函数
   */
  public readonly unsubscribe = (
    eventName: WSReceptionEnum,
    fn: () => void
  ) => {
    this._socket?.off(eventName, fn);
  };

  /**
   * @description 发送一个ws事件
   * @param eventName ws标识
   * @param fn 回调函数
   */
  public readonly dispatch = (eventName: WSDispatchEnum, data: any) => {
    this._socket?.emit(eventName, data);
  };
}

export const ws = new WS() as WSExample;

export default {
  install: (app: App) => {
    app.config.globalProperties.$ws = ws;
  }
};
