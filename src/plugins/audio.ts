import { Howl, type HowlOptions } from "howler";

class Audio {
  public audio;
  private _currentAudioId: number | undefined; // 只使用一个播放器，保留唯一id
  constructor(options: HowlOptions) {
    this.audio = new Howl(options);

    this.audio.on("load", () => {
      console.log("load");
    });
  }

  public getAudio() {
    return this.audio;
  }

  public play() {
    if (this._currentAudioId) {
      this.audio.play(this._currentAudioId);
    } else {
      this._currentAudioId = this.audio.play();
    }
  }

  public pause() {
    if (this._currentAudioId) {
      this.audio.pause(this._currentAudioId);
    } else {
      this.audio.pause();
    }
  }

  public stop() {
    this.audio.stop();
  }

  public isPlaying() {
    if (this._currentAudioId) {
      return this.audio.playing(this._currentAudioId);
    } else return this.audio.playing();
  }

  public setVolume(volume: number) {
    this.audio.volume(volume);
  }

  public onSeek(callback: () => void, id?: number) {
    this.audio.on("seek", callback);
  }
}

export default Audio;
