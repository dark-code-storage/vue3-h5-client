import {
  createRouter,
  createWebHistory,
  type RouteLocationNormalized
} from "vue-router";
import routes from "./routes";
import setPageTitle from "@/utils/set-page-title";
import NProgress from "@/utils/progress";
import { useCachedViewStore } from "@/store/modules/cachedView";
import Token from "@/utils/token";

const router = createRouter({
  history: createWebHistory(),
  routes
});

export interface toRouteType extends RouteLocationNormalized {
  meta: RouterMetaConfig;
}

router.beforeEach((to: toRouteType, from, next) => {
  NProgress.start();
  // 检查是否有token
  if (!Token.getToken() && to.fullPath !== "/login") {
    // 没有token，跳转到登录页
    next({ path: "/login", replace: true });
  } else {
    // 路由缓存
    useCachedViewStore().addCachedView(to);
    // 页面 title
    setPageTitle(to.meta.title);

    next();
  }
});

router.afterEach(() => {
  NProgress.done();
});

export default router;
