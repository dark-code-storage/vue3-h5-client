import type { RouteRecordRaw } from "vue-router";
import Layout from "@/layout/index.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "root",
    component: Layout,
    redirect: { name: "Home" },
    children: [
      {
        path: "home",
        name: "Home",
        component: () => import("@/pages/home/index.vue"),
        meta: {
          title: "主页",
          isHideNavBar: true
        } as RouterMetaConfig
      },
      {
        path: "about",
        name: "About",
        component: () => import("@/pages/about/index.vue"),
        meta: {
          title: "关于"
        } as RouterMetaConfig
      },
      {
        path: "login",
        name: "Login",
        component: () => import("@/pages/login/index.vue"),
        meta: {
          noCache: true,
          title: "登录"
        } as RouterMetaConfig
      },
      {
        path: "/:path(.*)*",
        name: "NotFound",
        component: () => import("@/pages/page404/index.vue"),
        meta: {
          title: "访问页面不存在页",
          isHideNavBar: true,
          noCache: true
        } as RouterMetaConfig
      }
    ]
  }
];

export default routes;
