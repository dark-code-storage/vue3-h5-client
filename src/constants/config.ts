/**
 * @description token 用于存储的 Key
 */
export const TOKEN_KEY = "token";
/**
 * @description 浏览器storage 存储加密key
 */
export const STORAGE_CRYPTO_KEY = "csgo";
/**
 * @description 浏览器storage key前缀
 */
export const STORAGE_PREFIX = "csgo";
/**
 * @description 浏览器标题
 */
export const PAGE_DEFAULT_TITLE = "csgo";
/**
 * @description 语言
 */
export const LANGUAGE_KEY = "language";
/**
 * @description 主题Key
 */
export const THEME_MODE_KEY = "theme_mode";

/** 环境配置 */
/**
 * @description WS地址
 */
export const WS_URL = import.meta.env.VITE_WS_URL;
/**
 * @description WS路径
 */
export const WS_PATH = import.meta.env.VITE_WS_PATH;
/**
 * @description api请求地址
 */
export const BASE_API_URL = import.meta.env.VITE_BASE_API;
