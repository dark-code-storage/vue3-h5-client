import { http } from "@/utils/request";

// 用户登录
export const userLogin = (data: UserLoginParams) => {
  return http.post<UserLoginParams, UserLoginResult>("/api/user/login", data);
};

//获取用户信息
export const getUserInfo = (params: GetUserInfoParams) => {
  return http.get("/api/user/getUserInfo", params);
};
