import { store } from "@/store";
import { defineStore } from "pinia";
import { initLang } from "@/locales";
import { LANGUAGE_KEY, THEME_MODE_KEY } from "@/constants/config";
import { getStorage, setStorage } from "@/utils/storage";
import { LocaleEnum, ThemeEnum } from "@/enums/optionsEnum";

/**
 * @description 是否为暗夜模式
 * @returns boolean
 */
const isDarkMode = (): boolean => {
  const themeMode = getStorage(THEME_MODE_KEY);
  if (themeMode) {
    return themeMode === "true";
  } else {
    // 获取设备是否为暗色主题
    return window.matchMedia("(prefers-color-scheme: dark)").matches;
  }
};

export const useThemeModeStore = defineStore({
  id: "config",
  state: () => ({
    themeMode: isDarkMode(),
    language: initLang
  }),
  actions: {
    toggleThemeMode() {
      this.themeMode = !this.themeMode;
      if (this.themeMode) {
        document.documentElement.classList.add(ThemeEnum.DARK);
        setStorage(THEME_MODE_KEY, "true");
      } else {
        document.documentElement.classList.remove(ThemeEnum.DARK);
        setStorage(THEME_MODE_KEY, "false");
      }
    },
    toggleLanguage(lang: LocaleEnum) {
      this.language = lang;
      setStorage(LANGUAGE_KEY, lang);
    }
  }
});

export function useConfigStoreHook() {
  return useThemeModeStore(store);
}
