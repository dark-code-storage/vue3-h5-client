interface RequestResult<T> extends Promise {
  code: number;
  data: T;
  msg: string;
}
