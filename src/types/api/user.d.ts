interface UserLoginParams {
  username: string;
  password: string;
}

interface UserLoginResult {
  token: string;
}

interface GetUserInfoParams {}
