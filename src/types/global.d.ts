import type { WSDispatchEnum } from "@/enums/wsEnum";
import type { RouteMeta } from "vue-router";

/**
 * @description 全局ts类型
 */
declare global {
  /**
   * @description 路由meta配置
   */
  interface RouterMetaConfig extends RouteMeta {
    title?: string; //标题
    isHideNavBar?: boolean; //是否隐藏navbar
    noCache?: boolean; // 是否 不缓存路由 name 作为唯一标识
  }

  /**
   * @description ws实例
   */
  interface WSExample {
    subscribe: (eventName: WSReceptionEnum, fn: (...arg: any) => void) => void;
    unsubscribe: (
      eventName: WSReceptionEnum,
      fn: (...arg: any) => void
    ) => void;
    dispatch: (eventName: WSDispatchEnum, data: any) => void;
  }

  /**
   * @description  Storage 缓存配置
   */
  export interface StorageConfig {
    prefix: string;
    expire: number;
    isEncrypt: boolean;
  }
  /**
   * @description  Storage 类型
   */
  type StorageType = "localStorage" | "sessionStorage";
  /**
   * @description Storage 返回值
   */
  type StorageValue<T> = T | null | undefined;
}
